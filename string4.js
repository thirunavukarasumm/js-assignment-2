// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}




function isArrayOrString(object){
    return object instanceof Object && (object.constructor == String || object.constructor == Array);
  }


function titleCaseConverter(objectInput){
    if((objectInput instanceof Object && !(isArrayOrString(objectInput)))){
        let resultString = "";
        for (key of Object.keys(objectInput)){
            resultString += objectInput[key][0].toUpperCase() + objectInput[key].slice(1,objectInput[key].length).toLowerCase() + " ";
        }
        return resultString.trim();

    }else{
        return 0;
    }

}
module.exports = titleCaseConverter;
