// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.


function isDictionaryOrArray(object){
    return object instanceof Object && (object.constructor == Object || object.constructor == Array);
  // ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.


function isDictionaryOrArray(object){
    return object instanceof Object && (object.constructor == Object || object.constructor == Array);
  }


function splitIPAddress(stringInput){
    if((stringInput instanceof Object && !(isDictionaryOrArray(stringInput))) || typeof stringInput == "string"){
        // match() method is used to check whether there are any alphabets as IPv6 will have some of them.
        if((stringInput.match(/[a-zA-Z]/g) == null) && !(stringInput.includes(":")) && stringInput.length > 0){
            return stringInput.split(".").map(Number);
        }
        else{
            return [];
        }
            
    }
        else{
            return [];
        }
        
    }



module.exports = splitIPAddress;

// (stringInput.match(/[a-zA-Z]/g) == null)}


function splitIPAddress(stringInput){
    if((stringInput instanceof Object && !(isDictionaryOrArray(stringInput))) || typeof stringInput == "string"){
        // match() method is used to check whether there are any alphabets as IPv6 will have some of them.
        if((stringInput.match(/[a-zA-Z]/g) == null) && !(stringInput.includes(":")) && stringInput.length > 0){
            return stringInput.split(".").map(Number);
        }
        else{
            return [];
        }
            
    }
        else{
            return [];
        }
        
    }


}
module.exports = splitIPAddress;

