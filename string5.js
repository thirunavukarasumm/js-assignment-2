// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.




function arrayToSentence(arrayInput){
    if(Array.isArray(arrayInput) && arrayInput.length > 0){
        let resultString = "";
        for(let i=0; i<arrayInput.length;i++){
            if(i === arrayInput.length - 1){
                resultString += arrayInput[i] + ".";
            }else{
                resultString += arrayInput[i] + " ";
            }
            
        }
        return resultString;
    }else{
        return [];
    }
}

module.exports = arrayToSentence;