// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

function isDictionaryOrArray(object){
    return object instanceof Object && (object.constructor == Object || object.constructor == Array);
  }


function monthIdentifier(stringInput){
    if((stringInput instanceof Object && !(isDictionaryOrArray(stringInput))) || typeof stringInput == "string"){

        if(!(isNaN(Number(stringInput.replaceAll("/","")))) && stringInput.includes("/")){
            let months = {
                1: "January",
                2: "February",
                3:"March",
                4:"April",
                5:"May",
                6:"June",
                7:"July",
                8:"August",
                9:"September",
                10:"October",
                11:"November",
                12:"December"
            };
        return months[Number(stringInput.split("/")[1])];
        }else {
            return [];
        }
    }else{
        return [];
    }
}
module.exports = monthIdentifier;