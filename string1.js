// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. There could be typing mistakes in the string so if the number is invalid, return 0. 

// To check if the input is an Object or Array.
function isDictionaryOrArray(object){
    return object.constructor == Object || object.constructor == Array;
  }

// To remove the special characters from the string.
function removeSpecialCharacters(stringInput){
    let invalidSpecialCharacters = ["!","@","#","%","*","/",";",":","&"];
    for(let i = 0;i < invalidSpecialCharacters.length;i++){
        if(stringInput.includes(invalidSpecialCharacters[i])){
            return 0;
        }
    }
    
    if(stringInput.includes("-")){
        if (stringInput.indexOf("-") != 0){
            return 0;
        }
        if(stringInput.includes("$")){
            if (stringInput.indexOf("$") != 1){
                return 0;
            }else{
                stringInput = stringInput.replace("$","");
            }
        }
    }else{
        if(stringInput.includes("$")){
            return 0;
        }
    }   
    return stringInput;
        
}

// Type converting function.
function typeConverter(stringInput){
    if((stringInput instanceof Object && !(isDictionaryOrArray(stringInput))) || typeof stringInput == "string"){
        if((stringInput.match(/-/g) || []).length > 1 || (stringInput.match(/\$/g) || []).length > 1 || (stringInput.match(/\./g) || []).length > 1){
            return 0;
        }else{
            let resultString = removeSpecialCharacters(stringInput);
            if (resultString == 0){
                return 0;
            }else{
                if(isNaN(Number(resultString))){
                    return 0;
                    }else{
                        return resultString;
                    }
            }
            
        }
        
    }  
    else{
        return 0;
    }
    

}


module.exports = typeConverter;







